﻿using DemoCapas.BOL;
using DemoCapas.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoCapas.GUI
{
    public partial class FrmUsuario : Form
    {
        private UsuarioBOL ubo;
        public EUsuario Usuario { get; set; }
        public bool IsAdmin { get; set; }

        public FrmUsuario()
        {
            InitializeComponent();
        }

        private void FrmUsuario_Load(object sender, EventArgs e)
        {
            ubo = new UsuarioBOL();
            cbxTipos.DataSource = ubo.CargarTipos();
            cbxTipos.Enabled = IsAdmin;
            cbxTipos.SelectedIndex = 1;

            if (Usuario?.Id > 0)
            {
                txtNombre.Text = Usuario.Nombre;
                txtUsuario.Text = Usuario.Usuario;
                txtEmail.Text = Usuario.Email;
                txtPass.Text = Usuario.Password;
                txtRePass.Text = IsAdmin ? Usuario.Password : "";
                pbxFoto.Image = Usuario?.Foto?.Imagen;
                cbxTipos.SelectedValue = Usuario?.TipoUsuario?.Id;
            }
        }

        private void SubirFoto(object sender, EventArgs e)
        {
            try
            {
                if (fdFoto.ShowDialog() == DialogResult.OK)
                {
                    pbxFoto.Image = Image.FromFile(fdFoto.FileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                EUsuario usuario = new EUsuario
                {
                    Id = Usuario == null ? 0:Usuario.Id,
                    Nombre = txtNombre.Text.Trim(),
                    Usuario = txtUsuario.Text.Trim(),
                    Email = txtEmail.Text.Trim(),
                    Password = txtPass.Text.Trim(),
                    Foto = new EFoto() { Imagen = pbxFoto.Image },
                    TipoUsuario = (ETipoUsuario)cbxTipos.SelectedItem,
                    Activo = true
                };
                string repass = txtRePass.Text.Trim();
                ubo.Guardar(usuario, repass);
                MessageBox.Show("Registrado exitosamente!!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
