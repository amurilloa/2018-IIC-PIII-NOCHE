using DemoCapas.BOL;
using DemoCapas.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoCapas.GUI
{
    public partial class FrmLogin : Form
    {
        private UsuarioBOL ubo;

        public FrmLogin()
        {
            InitializeComponent();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                EUsuario u = new EUsuario
                {
                    Usuario = txtUsuario.Text.Trim(),
                    Password = txtPassword.Text.Trim()
                };
                u = ubo.Login(u);

                if (u.Id > 0)
                {
                    Hide();
                    new FrmPrincipal() { Usuario = u}.ShowDialog();
                    Show();
                }
                else
                {
                    MessageBox.Show("Fail!!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            ubo = new UsuarioBOL();
        }

        private void lblRegisrar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Hide();
             new FrmUsuario().ShowDialog();
            Show();
        }
    }
}