﻿using DemoCapas.BOL;
using DemoCapas.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoCapas.GUI
{
    public partial class FrmUsuariosAdm : Form
    {
        private UsuarioBOL ubo;
        public EUsuario Logueado { get; set; }

        public FrmUsuariosAdm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Refrescar();
        }

        private void Refrescar()
        {
            dgvUsuarios.DataSource = ubo.CargarUsuarios(txtFiltro.Text.Trim());

        }
        private void FrmUsuariosAdm_Load(object sender, EventArgs e)
        {
            ubo = new UsuarioBOL();
        }

        private void Filtrar(object sender, KeyEventArgs e)
        {
            if (txtFiltro.Text.Trim().Length >= 3)
            {
                Refrescar();
            }
            else
            {
                dgvUsuarios.DataSource = null;
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FrmUsuario frmUsuario = new FrmUsuario { IsAdmin = Logueado.TipoUsuario.Id == (int)TipoUsuario.USU_ADM };
            Hide();
            frmUsuario.ShowDialog();
            Show();
        }

        private void Editar(object sender, EventArgs e)
        {
            if (dgvUsuarios.SelectedRows.Count > 0)
            {
                FrmUsuario frmUsuario = new FrmUsuario
                {
                    IsAdmin = Logueado.TipoUsuario.Id == (int)TipoUsuario.USU_ADM,
                    Usuario = (EUsuario)dgvUsuarios.SelectedRows[0].DataBoundItem

                };
                Hide();
                frmUsuario.ShowDialog();

                Show();
            }
            else
            {
                MessageBox.Show("Favor seleccionar un usuario.");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dgvUsuarios.SelectedRows.Count > 0)
            {
                EUsuario Usuario = (EUsuario)dgvUsuarios.SelectedRows[0].DataBoundItem;
                string texto = String.Format("Está seguro  que desea eliminar a {0}", Usuario.Nombre);
                if (MessageBox.Show(texto, "Eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    ubo.Eliminar(Usuario);
                    Refrescar();
                }
            }
        }
    }
}
