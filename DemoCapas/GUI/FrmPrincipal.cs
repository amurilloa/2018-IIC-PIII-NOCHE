﻿using DemoCapas.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoCapas.GUI
{
    public partial class FrmPrincipal : Form
    {
        public EUsuario Usuario { get; set; }
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            lblUsuario.Text = Usuario.Usuario;
            pictureBox1.Image = Usuario.Foto.Imagen;
            menuAdmin.Visible = Usuario.TipoUsuario.Id == (int)TipoUsuario.USU_ADM;
        }

        private void EditarPerfil(object sender, EventArgs e)
        {
            Hide();
            new FrmUsuario() { Usuario = Usuario }.ShowDialog();
            Show();
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FrmUsuariosAdm() { Logueado = Usuario}.Show();
        }
    }
}
