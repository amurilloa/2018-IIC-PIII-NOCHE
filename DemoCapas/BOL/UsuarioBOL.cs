﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoCapas.DAL;
using DemoCapas.Entities;

namespace DemoCapas.BOL
{
    class UsuarioBOL
    {
        private UsuarioDAL udal;

        public UsuarioBOL()
        {
            udal = new UsuarioDAL();
        }

        internal EUsuario Login(EUsuario u)
        {
            if (String.IsNullOrEmpty(u.Password) || String.IsNullOrEmpty(u.Usuario))
            {
                throw new Exception("Usuario y contraseña requeridos");
            }


            if (!ValidarPassword(u.Password))
            {
                throw new Exception("Credenciales inválidas");
            }

            return udal.Login(u);
        }

        internal List<EUsuario> CargarUsuarios(string filtro = "")
        {
            return udal.CargarUsuarios(filtro);
        }

        internal void Guardar(EUsuario u, string repass)
        {
            if (String.IsNullOrEmpty(u.Password) || String.IsNullOrEmpty(u.Usuario)
                || String.IsNullOrEmpty(u.Nombre) || String.IsNullOrEmpty(u.Email))
            {
                throw new Exception("Datos requeridos");
            }

            if (!ValidarPassword(u.Password))
            {
                throw new Exception("Constraseña no cumple con las normas de seguridad");
            }

            if (!u.Password.Equals(repass))
            {
                throw new Exception("Contraseñas no coinciden");
            }
            if (u.Id > 0)
            {
                udal.Modificar(u);
            }
            else
            {
                udal.Insertar(u);
            }
        }

        internal List<ETipoUsuario> CargarTipos()
        {
            return udal.CargarTiposUsuario(false);
        }

        private bool ValidarPassword(string pass)
        {
            //TODO: todas las demás caracterisiticas de seguridad de la contraseña
            return pass.Length > 8;
        }

        internal void Eliminar(EUsuario usuario)
        {
            udal.Eliminar(usuario);
        }
    }
}
