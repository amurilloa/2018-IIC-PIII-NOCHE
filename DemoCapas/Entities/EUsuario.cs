﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoCapas.Entities
{
    public class EUsuario
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Usuario { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public EFoto Foto { get; set; }
        public ETipoUsuario TipoUsuario{ get; set; }
        public bool Activo { get; set; }
        private Image imagen;

        public Image Imagen
        {
            get { return Foto?.Imagen; }
        }


    }
}
