﻿CREATE DATABASE progra_db;

CREATE TABLE usuarios
(
    id serial PRIMARY KEY,
    nombre text NOT NULL,
    usuario text NOT NULL UNIQUE,
    email text NOT NULL UNIQUE,
    pass text NOT NULL
);

create table foto(
	id serial primary, 
	foto bytea not null
);

ALTER TABLE usuarios
    ADD COLUMN id_foto integer;
	
ALTER TABLE usuarios
    ADD CONSTRAINT id_foto FOREIGN KEY (id_foto)
    REFERENCES foto (id)

create table tipo_usuarios
(
	id serial primary key,
	tipo text not null, 
	activo bool default true
);
insert into tipo_usuarios(tipo) values ('Administrador');
insert into tipo_usuarios(tipo) values ('Usuario');


ALTER TABLE usuarios
    ADD COLUMN id_tip_usu integer default 2;
	
ALTER TABLE usuarios
    ADD CONSTRAINT fk_tip_usu FOREIGN KEY (id_tip_usu)
    REFERENCES tipo_usuarios (id);
	
ALTER TABLE usuarios
    ADD COLUMN activo bool default true;