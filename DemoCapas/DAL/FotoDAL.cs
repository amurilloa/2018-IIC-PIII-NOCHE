﻿using DemoCapas.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoCapas.DAL
{
    class FotoDAL
    {
        internal int Insertar(EFoto f)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "INSERT INTO foto(imagen) VALUES (@ima) returning id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                MemoryStream stream = new MemoryStream();
                f.Imagen.Save(stream, ImageFormat.Jpeg);
                cmd.Parameters.AddWithValue("@ima", stream.ToArray());
                object id = cmd.ExecuteScalar();
                return id == DBNull.Value ? 0 : Convert.ToInt32(id);
            }
        }

        internal EFoto CargarId(int id)
        {
            EFoto foto = new EFoto();
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "select id, imagen from foto where id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    foto.Id = reader.GetInt32(0);
                    byte[] f = new byte[0];
                    f = (byte[])reader["imagen"];
                    MemoryStream stream = new MemoryStream(f);
                    foto.Imagen = Image.FromStream(stream);
                }
            }
            return foto;
        }
    }
}
