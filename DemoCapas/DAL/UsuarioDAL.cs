﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoCapas.Entities;
using Npgsql;

namespace DemoCapas.DAL
{
    class UsuarioDAL
    {
        internal EUsuario Login(EUsuario u)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "select  id, nombre, usuario, email, pass, id_foto, id_tip_usu, activo from usuarios" +
                    " where usuario = @usu and pass = @pas and activo = true";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@usu", u.Usuario);
                cmd.Parameters.AddWithValue("@pas", u.Password);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    u = CargarUsuario(reader);
                }
            }
            return u;
        }

        internal List<EUsuario> CargarUsuarios(string filtro)
        {
            List<EUsuario> usuarios = new List<EUsuario>();
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "select id, nombre, usuario, email, pass, id_foto, id_tip_usu, activo from usuarios " +
                    " order by nombre asc";

                if (!String.IsNullOrEmpty(filtro))
                {
                    sql = " SELECT id, nombre, usuario, email, pass, id_foto, id_tip_usu, activo " +
                        "FROM public.usuarios " +
                        "WHERE lower(nombre) like lower(@par) " +
                        "OR lower(usuario) like lower(@par)" +
                        "OR lower(email) like lower(@par)";
                }

                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                if (!String.IsNullOrEmpty(filtro))
                {
                    cmd.Parameters.AddWithValue("@par", filtro + "%");
                }
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    
                    
                    usuarios.Add(CargarUsuario(reader));
                }

            }
            return usuarios;
        }

        internal void Eliminar(EUsuario u)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "UPDATE usuarios " +
                    " SET activo = false " +
                    " WHERE id =@id ";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", u.Id);
                cmd.ExecuteNonQuery();
            }
        }

        private EUsuario CargarUsuario(NpgsqlDataReader reader)
        {
            FotoDAL fdal = new FotoDAL();
            EUsuario user = new EUsuario
            {
                Id = reader.GetInt32(0),
                Nombre = reader.GetString(1),
                Usuario = reader.GetString(2),
                Email = reader.GetString(3),
                Password = reader.GetString(4),
                Foto = reader[5] == DBNull.Value ? new EFoto()
                        : fdal.CargarId(reader.GetInt32(5)),
                TipoUsuario = CargarTiposUsuarioId(reader.GetInt32(6)),
                Activo = reader.GetBoolean(7)
            };
            return user;
        }

        internal void Insertar(EUsuario u)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "INSERT INTO public.usuarios(nombre, usuario, email, pass, id_foto, id_tip_usu)" +
                    " VALUES(@nom, @usu, @ema, @pas, @fot, @tip)";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@nom", u.Nombre);
                cmd.Parameters.AddWithValue("@usu", u.Usuario);
                cmd.Parameters.AddWithValue("@ema", u.Email);
                cmd.Parameters.AddWithValue("@pas", u.Password);
                //Insertar la foto, para recuperar el ID
                FotoDAL fdal = new FotoDAL();
                object idFoto = fdal.Insertar(u.Foto);
                cmd.Parameters.AddWithValue("@fot", idFoto == (object)0 ? DBNull.Value : idFoto);
                cmd.Parameters.AddWithValue("@tip", u.TipoUsuario.Id == 0 ? DBNull.Value : (object)u.TipoUsuario.Id);
                cmd.ExecuteNonQuery();
            }
        }

        internal void Modificar(EUsuario u)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "UPDATE usuarios " +
                    " SET nombre =@nom, usuario =@usu, email =@ema, pass =@pas, id_foto =@fot, id_tip_usu = @tip" +
                    " WHERE id =@id ";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@nom", u.Nombre);
                cmd.Parameters.AddWithValue("@usu", u.Usuario);
                cmd.Parameters.AddWithValue("@ema", u.Email);
                cmd.Parameters.AddWithValue("@pas", u.Password);
                //Insertar la foto, para recuperar el ID
                FotoDAL fdal = new FotoDAL();
                object idFoto = fdal.Insertar(u.Foto);
                cmd.Parameters.AddWithValue("@fot", idFoto == (object)0 ? DBNull.Value : idFoto);
                cmd.Parameters.AddWithValue("@tip", u.TipoUsuario.Id);
                cmd.Parameters.AddWithValue("@id", u.Id);
                cmd.ExecuteNonQuery();
            }
        }

        internal List<ETipoUsuario> CargarTiposUsuario(bool activos)
        {
            List<ETipoUsuario> tipos = new List<ETipoUsuario>();
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "select id, tipo, activo from tipo_usuarios ";
                if (activos)
                {
                    sql += " where activo = true";
                }
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ETipoUsuario tipo = new ETipoUsuario
                    {
                        Id = reader.GetInt32(0),
                        Tipo = reader.GetString(1),
                        Activo = reader.GetBoolean(2)
                    };
                    tipos.Add(tipo);
                }

            }
            return tipos;
        }

        private ETipoUsuario CargarTiposUsuarioId(int id)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "select id, tipo, activo from tipo_usuarios where id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    ETipoUsuario tipo = new ETipoUsuario
                    {
                        Id = reader.GetInt32(0),
                        Tipo = reader.GetString(1),
                        Activo = reader.GetBoolean(2)
                    };
                    return tipo;
                }
            }
            return new ETipoUsuario();
        }

    }
}