﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_4
{
    class Gasolinera
    {
        const double LIT_X_GAL = 3.78541;

        public double PrecioLitro { get; set; }

        public Gasolinera()
        {
            PrecioLitro = 680;
        }

        public double Cobro(double galones)
        {
            return galones * LIT_X_GAL * PrecioLitro;
        }
    }
}
