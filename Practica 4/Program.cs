﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_4
{
    class Program
    {
        //static T Leer<T>(string mensaje = "Digite un número ") where T : class
        //{
        //    FALLO:
        //    Console.Write(mensaje);
        //    if (!T.TryParse(Console.ReadLine(), out T num))
        //    {
        //        Console.Write("Intente nuevamente, ");
        //        mensaje = mensaje.ToLower();
        //        goto FALLO;
        //    }
        //    return num;
        //}

        static int LeerInt(string mensaje = "Digite un número ")
        {
            FALLO:
            Console.Write(mensaje);
            if (!Int32.TryParse(Console.ReadLine(), out int num))
            {
                Console.Write("Intente nuevamente, ");
                mensaje = mensaje.ToLower();
                goto FALLO;
            }
            return num;
        }

        static double LeerDouble(string mensaje = "Digite un número ")
        {
            FALLO:
            Console.Write(mensaje);
            if (!Double.TryParse(Console.ReadLine(), out double num))
            {
                Console.Write("Intente nuevamente, ");
                mensaje = mensaje.ToLower();
                goto FALLO;
            }
            return num;
        }
        static void Main(string[] args)
        {
            Logica log = new Logica();
            string menu = "Menu Principal\n" +
                "1. Circunferencia\n" +
                "2. Rectángulo\n" +
                "3. Fecha\n" +
                "4. Articulo\n" +
                "5. Temperatura\n" +
                "6. Divisas\n" +
                "7. Gasolinera\n" +
                "8. Restaurante\n" +
                "9. Automovil\n" +
                "10. Segundos\n" +
                "11. Llamada Telefónica\n" +
                "12. Salir\n" +
                "Seleccione una opción: ";
            string menuR = "Restaurante UTN\n" +
                "1. Capturar Orden\n" +
                "2. Calcular Cuenta\n" +
                "3. Volver\n" +
                "Seleccione una opción: ";
            int op = 0;
            do
            {
                PRINCIPAL:
                Console.Clear();
                op = LeerInt(menu);
                switch (op)
                {
                    case 1:
                        Circunferencia moneda = new Circunferencia { Radio = 1.4 };
                        Circunferencia rueda = new Circunferencia { Radio = 10.2 };
                        Console.WriteLine("Área de la moneda: {0:F2}", moneda.CalcularArea());
                        Console.WriteLine("Área de la rueda: {0:F2}", rueda.CalcularArea());
                        Console.WriteLine("Perímetro de la moneda: {0:F2}", moneda.CalcularPerimetro());
                        Console.WriteLine("Perímetro de la rueda: {0:F2}", rueda.CalcularPerimetro());
                        break;
                    case 2:
                        Rectangulo pared = new Rectangulo
                        {
                            Largo = LeerDouble("Digite el alto de la pared, en metros: "),
                            Ancho = LeerDouble("Digite el ancho de la pared, en metros: ")
                        };
                        Rectangulo ventana = new Rectangulo
                        {
                            Largo = LeerDouble("Digite el alto de la ventana, en metros: "),
                            Ancho = LeerDouble("Digite el ancho de la ventana, en metros: ")
                        };
                        double min = log.Pintar(pared, ventana);
                        Console.WriteLine("Tiempo para pintar: {0:F2}", log.Convertir(min));
                        break;
                    case 3:
                        Fecha fecha = new Fecha();
                        Console.WriteLine(fecha.FechaLarga());
                        fecha = new Fecha(28, 2, 1999);
                        Console.WriteLine(fecha.FechaLarga());
                        fecha = new Fecha(29, 2, 1999);
                        Console.WriteLine(fecha.FechaLarga());
                        fecha = new Fecha(31, 12, 2018);
                        Console.WriteLine(fecha.FechaLarga());
                        break;
                    case 4:
                        Articulo art = new Articulo { Clave = 1, Cantidad = 10, Descripcion = "Confites", Precio = 50 };
                        Console.WriteLine("IVA = {0}", art.CalcularIVA());
                        break;
                    case 5:
                        Temperatura temp = new Temperatura { GradosC = 22 };
                        Console.WriteLine("{0}°C --> {1}°F", temp.GradosC, temp.ConvertirAFarenheit());
                        break;
                    case 6:
                        CambioDivisas cv = new CambioDivisas(569.97) { Colones = 125000 };
                        Console.WriteLine("{0:0.00} CRC son {1:0.00} USD", cv.Colones, cv.ConvertirADolares());
                        break;
                    case 7:
                        Gasolinera g = new Gasolinera();
                        double dis = 2.3;
                        Console.WriteLine("Por {0:F2} galones debe pagar {1:F2} colones", dis, g.Cobro(dis));
                        break;
                    case 8:
                        Mesa[] mesas = new Mesa[]
                        {
                            new Mesa(),
                            new Mesa(),
                            new Mesa(),
                            new Mesa(),
                            new Mesa()
                        };
                        do
                        {
                            Console.Clear();
                            op = LeerInt(menuR);
                            switch (op)
                            {
                                case 1:
                                    int selMesa = LeerInt("# Mesa: ");
                                    do
                                    {
                                        Console.Clear();
                                        Console.WriteLine("# Mesa: {0}", selMesa);
                                        int plato = LeerInt(mesas[selMesa - 1].DesplegarMenu(false) + "7. Volver\nSeleccione una opción: ");
                                        if (plato == 7)
                                        {
                                            break;
                                        }
                                        int cant = LeerInt("Cantidad deseada: ");
                                        mesas[selMesa - 1].AgregarPedido(plato - 1, cant);
                                    } while (true);
                                    break;
                                case 2:
                                    selMesa = LeerInt("# Mesa: ");
                                    Console.WriteLine("# Mesa: {0}\n{1}", selMesa, mesas[selMesa - 1].DesplegarMenu(true));
                                    mesas[selMesa - 1] = new Mesa();
                                    Console.ReadKey();
                                    break;
                                default:
                                    break;
                            }
                        } while (op != 3);
                        goto PRINCIPAL;
                    case 9:
                        Automovil auto = new Automovil { Costo = 20000 };
                        Console.WriteLine("El costo del vehículo es: {0:F2}", auto.PrecioFinal());
                        break;
                    case 10:
                        int d = 5;
                        ConvertirDias dias = new ConvertirDias { Dias = d };
                        Console.WriteLine("En {0} días hay {1} segundos", d, dias.ConvertirASegundos());
                        break;
                    case 11:
                        int m = 2;
                        Llamada tel = new Llamada { Tiempo = m };
                        Console.WriteLine("Por {0} min debe pagar {1}", m, tel.Cobrar());
                        break;
                    default:
                        break;
                }
                Console.ReadKey();
            } while (op != 12);
        }
    }
}
