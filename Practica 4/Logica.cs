﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_4
{
    class Logica
    {
        public double Pintar(Rectangulo pared, Rectangulo ventana)
        {
            return (pared.CalcularArea() - ventana.CalcularArea()) * 10;
        }

        public string Convertir(double minutos)
        {
            double horas = minutos / 60;
            minutos %= 60;
            return String.Format("{0:0}hrs {1:0}min", horas, minutos);
        }
    }
}
