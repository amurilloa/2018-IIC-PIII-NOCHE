﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_4
{
    class Fecha
    {
        public int Dia { get; private set; }
        public int Mes { get; private set; }
        public int Anno { get; private set; }

        public Fecha()
        {
            Dia = 1;
            Mes = 1;
            Anno = 2000;
        }

        public Fecha(int dia, int mes, int anno)
        {
            ModificarFecha(dia, mes, anno);
        }

        public string FechaCorta()
        {
            return String.Format("{0:00}/{1:00}/{2:0000}", Dia, Mes, Anno);
        }

        public string FechaLarga()
        {
            return String.Format("{0} de {1} de {2:0000}", Dia, ConvertirMes(Mes), Anno);
        }

        private string ConvertirMes(int mes)
        {
            string[] meses = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                "Julio", "Agosto", "Septiembre","Octubre", "Noviembre", "Diciembre" };
            return meses[mes - 1];
        }

        public void ModificarFecha(int dia, int mes, int anno)
        {
            if (Validar(dia, mes, anno))
            {
                Dia = dia;
                Mes = mes;
                Anno = anno;
            }
            else
            {
                Dia = 1;
                Mes = 1;
                Anno = 2000;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dia"></param>
        /// <param name="mes"></param>
        /// <param name="anno"></param>
        /// <returns></returns>
        public bool Validar(int dia, int mes, int anno)
        {
            //1-31 dias , 1-12 meses, año no sea negativo
            if (dia < 1 || dia > 31 || mes < 1 || mes > 12 || anno < 0)
            {
                return false;
            }
            if (dia > 30 && (mes == 4 || mes == 6 || mes == 9 || mes == 11))
            {
                return false;
            }
            bool bi = Bisiesto(anno);
            if (mes == 2 && ((dia > 28 && !bi) || (bi && dia > 29)))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="anno"></param>
        /// <returns></returns>
        public bool Bisiesto(int anno)
        {
            if (anno % 4 == 0)
            {
                if (anno % 100 == 0)
                {
                    return anno % 400 == 0;
                }
                return true;
            }
            return false;
        }
    }
}
