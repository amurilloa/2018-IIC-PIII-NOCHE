﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_4
{
    class CambioDivisas
    {
        public double Colones { get; set; }
        public double TipoCambio { get; set; }

        public CambioDivisas(double tipoCambio)
        {
            TipoCambio = tipoCambio;
        }

        public double ConvertirADolares()
        {
            return Colones / TipoCambio;
        }
    }
}
