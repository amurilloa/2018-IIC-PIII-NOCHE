﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_4
{
    class Temperatura
    {
        public double GradosC { get; set; }

        public double ConvertirAFarenheit()
        {
            return 9 * GradosC / 5 + 32;
        }
    }
}
