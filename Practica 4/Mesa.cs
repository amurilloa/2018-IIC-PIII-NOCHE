﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_4
{
    class Mesa
    {
        public Producto[] Menu { get; set; }
        public int[] Pedido { get; set; }
        public Mesa()
        {
            Menu = new Producto[]
            {
                new Producto(){Descripcion = "Hamburguesa sencilla...", Precio = 15 },
                new Producto(){Descripcion = "Hamburguesa con queso..", Precio = 18 },
                new Producto(){Descripcion = "Hamburguesa especial...", Precio = 20 },
                new Producto(){Descripcion = "Papas Fritas...........", Precio = 8 },
                new Producto(){Descripcion = "Fresco.................", Precio = 5 },
                new Producto(){Descripcion = "Postre.................", Precio = 6 }
                                              
            };
            Pedido = new int[Menu.Length];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="producto"></param>
        /// <param name="cantidad"></param>
        public void AgregarPedido(int producto, int cantidad)
        {
            Pedido[producto] += cantidad;
            if (Pedido[producto] < 0)
            {
                Pedido[producto] = 0;
            }
        }

        public double CalcularCuenta()
        {
            double total = 0;
            for (int i = 0; i < Menu.Length; i++)
            {
                total += (Menu[i].Precio * Pedido[i]);
            }
            return total;
        }

        public string DesplegarMenu(bool total)
        {
            string menu = "Restaurante UTN\n";
            for (int i = 0; i < Menu.Length; i++)
            {
                if (total && Pedido[i] == 0)
                {
                    continue;
                }
                menu += String.Format("{0}. {1}(${2})\t{3}\n",
                    !total ? (i + 1).ToString(): " ",
                    Menu[i].Descripcion,
                    Menu[i].Precio,
                    Pedido[i] == 0 ? "__" : Pedido[i].ToString());
            }
            if (total)
            {
                menu += String.Format("   Total.....................  ${0}\n", CalcularCuenta());
            }
            return menu;
        }
    }
}
