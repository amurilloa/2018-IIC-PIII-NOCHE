﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_4
{
    class ConvertirDias
    {
        public int Dias { get; set; }

        public int ConvertirASegundos()
        {
            return Dias * 24 * 60 * 60;
        }

    }
}
