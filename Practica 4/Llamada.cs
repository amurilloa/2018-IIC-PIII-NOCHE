﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_4
{
    class Llamada
    {
        private int tiempo;

        public int Tiempo
        {
            get { return tiempo; }
            set { tiempo = value >= 0 ? value : 0; }
        }

        public int Cobrar()
        {
            if (tiempo > 3)
            {
                return 5 + (tiempo - 3) * 3;
            }
            return tiempo == 0 ? 0 : 5;
        }
    }
}
