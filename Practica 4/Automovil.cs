﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_4
{
    class Automovil
    {
        public double Costo { get; set; }

        public double PrecioFinal()
        {
            double ga = Costo * 0.12;
            double pv = Costo + ga;
            double im = pv * 0.06;
            double pf = pv + im;
            return pf;
        }
    }
}
