﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisorImagenes.BO
{
    class VisorBO
    {
        private FileInfo[] files;
        private int index;

        internal bool CargarArchivos(string selectedPath)
        {
            DirectoryInfo di = new DirectoryInfo(selectedPath);
            files = di.GetFiles("*.jpg");
            index = -1;
            return files.Length > 0;
        }

        internal string Siguiente()
        {
            index++;
            if (index >= files.Length)
            {
                index = 0;
            }
            return files[index].FullName;
        }

        internal string Anterior()
        {
            index--;
            if (index < 0)
            {
                index = files.Length - 1;
            }
            return files[index].FullName;
        }
    }
}
