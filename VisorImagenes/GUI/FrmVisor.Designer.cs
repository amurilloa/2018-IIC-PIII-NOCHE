﻿namespace VisorImagenes.GUI
{
    partial class FrmVisor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVisor));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnSig = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.btnAnt = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.segundoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.segundosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.segundosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.segundosToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.segundosToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.btnSig);
            this.panel1.Controls.Add(this.btnPause);
            this.panel1.Controls.Add(this.btnPlay);
            this.panel1.Controls.Add(this.btnAnt);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(448, 874);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(706, 100);
            this.panel1.TabIndex = 1;
            // 
            // btnSearch
            // 
            this.btnSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSearch.BackgroundImage")));
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnSearch.Location = new System.Drawing.Point(509, 8);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(80, 80);
            this.btnSearch.TabIndex = 5;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnSig
            // 
            this.btnSig.BackgroundImage = global::VisorImagenes.Properties.Resources.btnSig;
            this.btnSig.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSig.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnSig.Enabled = false;
            this.btnSig.Location = new System.Drawing.Point(384, 8);
            this.btnSig.Name = "btnSig";
            this.btnSig.Size = new System.Drawing.Size(80, 80);
            this.btnSig.TabIndex = 3;
            this.btnSig.UseVisualStyleBackColor = true;
            this.btnSig.Click += new System.EventHandler(this.btnSig_Click);
            // 
            // btnPause
            // 
            this.btnPause.BackgroundImage = global::VisorImagenes.Properties.Resources.btnPause;
            this.btnPause.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPause.Enabled = false;
            this.btnPause.Location = new System.Drawing.Point(287, 8);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(80, 80);
            this.btnPause.TabIndex = 2;
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.BackgroundImage = global::VisorImagenes.Properties.Resources.btnPlay;
            this.btnPlay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPlay.Enabled = false;
            this.btnPlay.Location = new System.Drawing.Point(192, 8);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(80, 80);
            this.btnPlay.TabIndex = 1;
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // btnAnt
            // 
            this.btnAnt.BackgroundImage = global::VisorImagenes.Properties.Resources.btnAnt;
            this.btnAnt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAnt.Enabled = false;
            this.btnAnt.Location = new System.Drawing.Point(96, 8);
            this.btnAnt.Name = "btnAnt";
            this.btnAnt.Size = new System.Drawing.Size(80, 80);
            this.btnAnt.TabIndex = 0;
            this.btnAnt.UseVisualStyleBackColor = true;
            this.btnAnt.Click += new System.EventHandler(this.btnAnt_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.label1.Image = global::VisorImagenes.Properties.Resources.separador;
            this.label1.Location = new System.Drawing.Point(401, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 91);
            this.label1.TabIndex = 4;
            this.label1.Text = "      ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.ContextMenuStrip = this.contextMenuStrip1;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::VisorImagenes.Properties.Resources.IMG_9009;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1536, 995);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.segundoToolStripMenuItem,
            this.segundosToolStripMenuItem,
            this.segundosToolStripMenuItem1,
            this.segundosToolStripMenuItem2,
            this.segundosToolStripMenuItem3});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(217, 184);
            // 
            // segundoToolStripMenuItem
            // 
            this.segundoToolStripMenuItem.Name = "segundoToolStripMenuItem";
            this.segundoToolStripMenuItem.Size = new System.Drawing.Size(216, 36);
            this.segundoToolStripMenuItem.Tag = "1";
            this.segundoToolStripMenuItem.Text = "1 segundo";
            this.segundoToolStripMenuItem.Click += new System.EventHandler(this.AsignarTiempo);
            // 
            // segundosToolStripMenuItem
            // 
            this.segundosToolStripMenuItem.Name = "segundosToolStripMenuItem";
            this.segundosToolStripMenuItem.Size = new System.Drawing.Size(216, 36);
            this.segundosToolStripMenuItem.Tag = "2";
            this.segundosToolStripMenuItem.Text = "2 Segundos";
            this.segundosToolStripMenuItem.Click += new System.EventHandler(this.AsignarTiempo);
            // 
            // segundosToolStripMenuItem1
            // 
            this.segundosToolStripMenuItem1.Name = "segundosToolStripMenuItem1";
            this.segundosToolStripMenuItem1.Size = new System.Drawing.Size(216, 36);
            this.segundosToolStripMenuItem1.Tag = "3";
            this.segundosToolStripMenuItem1.Text = "3 segundos";
            this.segundosToolStripMenuItem1.Click += new System.EventHandler(this.AsignarTiempo);
            // 
            // segundosToolStripMenuItem2
            // 
            this.segundosToolStripMenuItem2.Name = "segundosToolStripMenuItem2";
            this.segundosToolStripMenuItem2.Size = new System.Drawing.Size(216, 36);
            this.segundosToolStripMenuItem2.Tag = "4";
            this.segundosToolStripMenuItem2.Text = "4 segundos";
            this.segundosToolStripMenuItem2.Click += new System.EventHandler(this.AsignarTiempo);
            // 
            // segundosToolStripMenuItem3
            // 
            this.segundosToolStripMenuItem3.Name = "segundosToolStripMenuItem3";
            this.segundosToolStripMenuItem3.Size = new System.Drawing.Size(216, 36);
            this.segundosToolStripMenuItem3.Tag = "5";
            this.segundosToolStripMenuItem3.Text = "5 segundos";
            this.segundosToolStripMenuItem3.Click += new System.EventHandler(this.AsignarTiempo);
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FrmVisor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1536, 995);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "FrmVisor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmVisor";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmVisor_FormClosing);
            this.Load += new System.EventHandler(this.FrmVisor_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnAnt;
        private System.Windows.Forms.Button btnSig;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem segundoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem segundosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem segundosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem segundosToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem segundosToolStripMenuItem3;
    }
}