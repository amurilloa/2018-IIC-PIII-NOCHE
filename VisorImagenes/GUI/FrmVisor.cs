﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisorImagenes.BO;

namespace VisorImagenes.GUI
{
    public partial class FrmVisor : Form
    {
        private VisorBO log;
       
        public FrmVisor()
        {
            InitializeComponent();
        }

        private void FrmVisor_Load(object sender, EventArgs e)
        {
            log = new VisorBO();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                HabilitarComponentes(log.CargarArchivos(folderBrowserDialog1.SelectedPath));
               
            }
        }

        private void HabilitarComponentes(bool v)
        {
            btnAnt.Enabled = v;
            btnPause.Enabled = v;
            btnPlay.Enabled = v;
            btnSig.Enabled = v;
        }

        private void btnSig_Click(object sender, EventArgs e)
        {
            Siguiente();
        }

        private void btnAnt_Click(object sender, EventArgs e)
        {
            Anterior();
        }

        private void Siguiente()
        {
            pictureBox1.Image = Image.FromFile(log.Siguiente());
        }

        private void Anterior()
        {
            pictureBox1.Image = Image.FromFile(log.Anterior());
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Siguiente();
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            timer1.Stop();
        }

        private void FrmVisor_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Stop();
        }

        private void AsignarTiempo(object sender, EventArgs e)
        {
            int tag = Int32.Parse(((ToolStripMenuItem)sender).Tag.ToString());
            Console.WriteLine(tag);
            timer1.Interval = tag * 1000;
        }
    }
}
