﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Logica
    {
        internal string UsuarioFecha()
        {
            return String.Format("Hola {0}, hoy es {1}",
                Environment.UserName, DateTime.Now.ToLongDateString());
        }

        internal int TresNumeros(int n1, int n2, int n3)
        {
            return n1 >= 0 ? n2 * n3 : n2 + n3;
        }

        internal int Invertir(int num)
        {
            int nuevo = 0;
            while (num != 0)
            {
                nuevo = nuevo * 10 + (num % 10);
                num /= 10;
            }
            return nuevo;
        }

        //TODO: Actualizar y hacer demo
        //internal (int, int, int) MinMaxMed(int[] datos)
        //{
        //    return (0, 0, 0);
        //}

        internal int Min(int[] datos)
        {
            int min = datos[0];
            foreach (int item in datos)
            {
                if (item < min)
                {
                    min = item;
                }
            }
            return min;
        }

        internal int Max(int[] datos)
        {
            int max = datos[0];
            foreach (int item in datos)
            {
                if (item > max)
                {
                    max = item;
                }
            }
            return max;
        }

        internal double Med(int[] datos)
        {
            int sumatoria = 0;
            foreach (int item in datos)
            {
                sumatoria += item;
            }
            return sumatoria / (double)datos.Length;
        }

        internal string Imprimir(int[] arreglo)
        {
            string datos = "";
            foreach (int item in arreglo)
            {
                datos += String.Format("{0} ", item);
            }
            return datos;
        }
    }
}
