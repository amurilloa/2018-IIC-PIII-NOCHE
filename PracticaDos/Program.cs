﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Program
    {
        static int LeerInt(string mensaje = "Digite un número ")
        {
            FALLO:
            Console.Write(mensaje);
            //int num;
            if (!Int32.TryParse(Console.ReadLine(), out int num))
            {
                Console.Write("Intente nuevamente, ");
                mensaje = mensaje.ToLower();
                goto FALLO;
            }
            return num;
        }

        static void Main(string[] args)
        {
            Logica log = new Logica();

            string menu = "Menu Principal\n" +
                "1. Usuario y Fecha\n" +
                "2. Tres números\n" +
                "3. LeerInt\n" +
                "4. Invertir\n" +
                "5. Grande, Pequeño, Media\n" +
                "6. Salir\n";
            int op = 0;

            do
            {
                Console.Clear();
                Console.Write(menu);
                op = LeerInt("Digite una opción: ");
                REPETIR:
                switch (op)
                {
                    case 1:
                        Console.WriteLine("\n" + log.UsuarioFecha());
                        break;
                    case 2:

                        int n1 = LeerInt("Digite el 1er número: ");
                        int n2 = LeerInt("Digite el 2do número: ");
                        int n3 = LeerInt("Digite el 3er número: ");
                        Console.WriteLine("El resultado es: {0}", log.TresNumeros(n1, n2, n3));
                        op = LeerInt("\n1 para continuar, 2 para repetir ");
                        if (op == 2)
                        {
                            Console.WriteLine();
                            goto REPETIR;
                        }
                        break;
                    case 3:
                        int x = LeerInt("Digite un número: ");
                        Console.WriteLine("El número leído es: {0}", x);
                        break;
                    case 4:
                        int num = LeerInt("Digite el número a invertir: ");
                        Console.WriteLine("{0} --> {1}", num, log.Invertir(num));
                        break;
                    case 5:
                        int largo = LeerInt("Digite el largo del arreglo: ");
                        int[] arreglo = new int[largo];
                        for (int i = 0; i < largo; i++)
                        {
                            arreglo[i] = LeerInt(String.Format("Digite el valor de la posición {0}: ", (i + 1)));
                        }
                        Console.WriteLine(log.Imprimir(arreglo));
                        Console.WriteLine("Mínimo: {0}\nMáximo: {1}\nMedia: {2}",
                            log.Min(arreglo), log.Max(arreglo), log.Med(arreglo));
                        break;
                    case 6:
                        goto end;
                    default:
                        break;
                }
                Console.ReadKey();
            }
            while (op != 6);
            end:
            Console.WriteLine("Gracias por utilizar la aplicación.");
            Console.ReadLine();
        }
    }
}
