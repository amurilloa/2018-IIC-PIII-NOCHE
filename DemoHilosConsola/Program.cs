﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DemoHilosConsola
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread hilo1 = new Thread(EscribirX);
            hilo1.Start();
            //EscribirX();
            EscribirY();
            Console.ReadKey();
        }


        static void EscribirX()
        {
            for (int i = 0; i < 100000000; i++)
            {
                Console.Write(".");
                Thread.Sleep(1);
            }
        }
        static void EscribirY()
        {
            for (int i = 0; i < 1000; i++)
            {
                Console.Write("O");
                Thread.Sleep(1);
            }
        }
    }
}
