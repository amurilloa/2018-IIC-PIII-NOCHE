﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SlotGameSuperMario
{
    public partial class Form1 : Form
    {
        private int count;
        public Form1()
        {
            InitializeComponent();
            count = 0;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Start();
            timer2.Start();
            timer3.Start();
            count = 0;
        }
   

        private void timer1_Tick(object sender, EventArgs e)
        {
            PictureBox[] arreglo = { pb1, pb2, pb3, pba1 };
            Mover(arreglo, true, 2);
        }


        private void Mover(PictureBox[] arreglo, bool dir, int dis)
        {
            foreach (PictureBox item in arreglo)
            {
                item.Location = new Point
                {
                    X = item.Location.X + (dir ? -dis : dis),
                    Y = item.Location.Y
                };
                if (dir && item.Location.X <= 0-item.Width)
                {
                    item.Location = new Point
                    {
                        X =  632,
                        Y = item.Location.Y
                    };
                } else if (!dir && item.Location.X >= 632)
                {
                    item.Location = new Point
                    {
                        X = 0 - item.Width,
                        Y = item.Location.Y
                    };
                }
            }
            Refresh();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            PictureBox[] arreglo = new PictureBox[] { pb4, pb5, pb6, pba2 };
            Mover(arreglo, false,6);
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            PictureBox[] arreglo = new PictureBox[] { pb7, pb8, pb9, pba3 };
            Mover(arreglo, true, 12);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (count == 0)
            {
                timer1.Stop();
                count++;
            }
            else if (count == 1)
            {
                timer2.Stop();
                count++;
            }else if (count == 2)
            {
                timer3.Stop();
                count++;
            }
        }
    }
}
