﻿namespace SlotGameSuperMario
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.pb9 = new System.Windows.Forms.PictureBox();
            this.pb8 = new System.Windows.Forms.PictureBox();
            this.pb7 = new System.Windows.Forms.PictureBox();
            this.pb6 = new System.Windows.Forms.PictureBox();
            this.pb5 = new System.Windows.Forms.PictureBox();
            this.pb4 = new System.Windows.Forms.PictureBox();
            this.pb3 = new System.Windows.Forms.PictureBox();
            this.pb2 = new System.Windows.Forms.PictureBox();
            this.pb1 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.pba3 = new System.Windows.Forms.PictureBox();
            this.pba2 = new System.Windows.Forms.PictureBox();
            this.pba1 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pb9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pba3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pba2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pba1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.875F);
            this.button1.Location = new System.Drawing.Point(121, 775);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 90);
            this.button1.TabIndex = 0;
            this.button1.Text = "Test";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pb9
            // 
            this.pb9.Image = global::SlotGameSuperMario.Properties.Resources.I9;
            this.pb9.ImageLocation = "";
            this.pb9.Location = new System.Drawing.Point(849, 512);
            this.pb9.Name = "pb9";
            this.pb9.Size = new System.Drawing.Size(425, 194);
            this.pb9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb9.TabIndex = 9;
            this.pb9.TabStop = false;
            // 
            // pb8
            // 
            this.pb8.Image = ((System.Drawing.Image)(resources.GetObject("pb8.Image")));
            this.pb8.ImageLocation = "";
            this.pb8.Location = new System.Drawing.Point(423, 512);
            this.pb8.Name = "pb8";
            this.pb8.Size = new System.Drawing.Size(425, 194);
            this.pb8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb8.TabIndex = 8;
            this.pb8.TabStop = false;
            // 
            // pb7
            // 
            this.pb7.Image = global::SlotGameSuperMario.Properties.Resources.I7;
            this.pb7.ImageLocation = "";
            this.pb7.Location = new System.Drawing.Point(1, 512);
            this.pb7.Name = "pb7";
            this.pb7.Size = new System.Drawing.Size(425, 194);
            this.pb7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb7.TabIndex = 7;
            this.pb7.TabStop = false;
            // 
            // pb6
            // 
            this.pb6.Image = global::SlotGameSuperMario.Properties.Resources.I6;
            this.pb6.ImageLocation = "";
            this.pb6.Location = new System.Drawing.Point(849, 321);
            this.pb6.Name = "pb6";
            this.pb6.Size = new System.Drawing.Size(425, 194);
            this.pb6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb6.TabIndex = 6;
            this.pb6.TabStop = false;
            // 
            // pb5
            // 
            this.pb5.Image = ((System.Drawing.Image)(resources.GetObject("pb5.Image")));
            this.pb5.ImageLocation = "";
            this.pb5.Location = new System.Drawing.Point(425, 321);
            this.pb5.Name = "pb5";
            this.pb5.Size = new System.Drawing.Size(425, 194);
            this.pb5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb5.TabIndex = 5;
            this.pb5.TabStop = false;
            // 
            // pb4
            // 
            this.pb4.Image = global::SlotGameSuperMario.Properties.Resources.I4;
            this.pb4.ImageLocation = "";
            this.pb4.Location = new System.Drawing.Point(1, 321);
            this.pb4.Name = "pb4";
            this.pb4.Size = new System.Drawing.Size(425, 194);
            this.pb4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb4.TabIndex = 4;
            this.pb4.TabStop = false;
            // 
            // pb3
            // 
            this.pb3.Image = global::SlotGameSuperMario.Properties.Resources.I3;
            this.pb3.ImageLocation = "";
            this.pb3.Location = new System.Drawing.Point(849, 128);
            this.pb3.Name = "pb3";
            this.pb3.Size = new System.Drawing.Size(425, 194);
            this.pb3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb3.TabIndex = 3;
            this.pb3.TabStop = false;
            // 
            // pb2
            // 
            this.pb2.Image = global::SlotGameSuperMario.Properties.Resources.I2;
            this.pb2.ImageLocation = "";
            this.pb2.Location = new System.Drawing.Point(425, 128);
            this.pb2.Name = "pb2";
            this.pb2.Size = new System.Drawing.Size(425, 194);
            this.pb2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb2.TabIndex = 2;
            this.pb2.TabStop = false;
            // 
            // pb1
            // 
            this.pb1.Image = global::SlotGameSuperMario.Properties.Resources.I1;
            this.pb1.ImageLocation = "";
            this.pb1.Location = new System.Drawing.Point(1, 128);
            this.pb1.Name = "pb1";
            this.pb1.Size = new System.Drawing.Size(425, 194);
            this.pb1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb1.TabIndex = 1;
            this.pb1.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 1;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // timer3
            // 
            this.timer3.Interval = 1;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // pba3
            // 
            this.pba3.Image = global::SlotGameSuperMario.Properties.Resources.I7;
            this.pba3.ImageLocation = "";
            this.pba3.Location = new System.Drawing.Point(1265, 512);
            this.pba3.Name = "pba3";
            this.pba3.Size = new System.Drawing.Size(425, 194);
            this.pba3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pba3.TabIndex = 14;
            this.pba3.TabStop = false;
            // 
            // pba2
            // 
            this.pba2.Image = global::SlotGameSuperMario.Properties.Resources.I4;
            this.pba2.ImageLocation = "";
            this.pba2.Location = new System.Drawing.Point(1265, 321);
            this.pba2.Name = "pba2";
            this.pba2.Size = new System.Drawing.Size(425, 194);
            this.pba2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pba2.TabIndex = 13;
            this.pba2.TabStop = false;
            // 
            // pba1
            // 
            this.pba1.Image = global::SlotGameSuperMario.Properties.Resources.I1;
            this.pba1.ImageLocation = "";
            this.pba1.Location = new System.Drawing.Point(1265, 128);
            this.pba1.Name = "pba1";
            this.pba1.Size = new System.Drawing.Size(425, 194);
            this.pba1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pba1.TabIndex = 12;
            this.pba1.TabStop = false;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.875F);
            this.button2.Location = new System.Drawing.Point(306, 775);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(179, 90);
            this.button2.TabIndex = 15;
            this.button2.Text = "Stop";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1702, 919);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.pba3);
            this.Controls.Add(this.pba2);
            this.Controls.Add(this.pba1);
            this.Controls.Add(this.pb9);
            this.Controls.Add(this.pb8);
            this.Controls.Add(this.pb7);
            this.Controls.Add(this.pb6);
            this.Controls.Add(this.pb5);
            this.Controls.Add(this.pb4);
            this.Controls.Add(this.pb3);
            this.Controls.Add(this.pb2);
            this.Controls.Add(this.pb1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pb9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pba3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pba2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pba1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pb1;
        private System.Windows.Forms.PictureBox pb2;
        private System.Windows.Forms.PictureBox pb3;
        private System.Windows.Forms.PictureBox pb6;
        private System.Windows.Forms.PictureBox pb5;
        private System.Windows.Forms.PictureBox pb4;
        private System.Windows.Forms.PictureBox pb9;
        private System.Windows.Forms.PictureBox pb8;
        private System.Windows.Forms.PictureBox pb7;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.PictureBox pba3;
        private System.Windows.Forms.PictureBox pba2;
        private System.Windows.Forms.PictureBox pba1;
        private System.Windows.Forms.Button button2;
    }
}

