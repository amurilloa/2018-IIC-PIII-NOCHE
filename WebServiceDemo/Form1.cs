﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using WebServiceDemo.WSBanco;
using WebServiceDemo.WSIMN;
using WebServiceDemo.WSAeropuertos;

namespace WebServiceDemo
{
    public partial class Form1 : Form
    {
        private double tc;

        public Form1()
        {
            InitializeComponent();
        }

        private void dtFecha_ValueChanged(object sender, EventArgs e)
        {
            ConsultarTipoCambio();
        }

        private void ConsultarTipoCambio()
        {
            try
            {
                wsIndicadoresEconomicosSoapClient ws =
                   new wsIndicadoresEconomicosSoapClient("wsIndicadoresEconomicosSoap");

                string xmlText = ws.ObtenerIndicadoresEconomicosXML("318", dtFecha.Value.ToString("dd/MM/yyyy"),
                    dtFecha.Value.ToString("dd/MM/yyyy"), "Allan - UTN", "N");

                /* XmlDocument xml = new XmlDocument();

                 xml.LoadXml(xmlText);

                 Console.WriteLine(xmlText);
                 */
                // tc = Double.Parse(xml.GetElementsByTagName("NUM_VALOR")[0].InnerText, CultureInfo.InvariantCulture);

                tc = xmlText.ParseXML<Datos_de_INGC011_CAT_INDICADORECONOMIC>().INGC011_CAT_INDICADORECONOMIC.NUM_VALOR;
                Console.WriteLine(tc);
            }
            catch (Exception)
            {
                MessageBox.Show("Problema al consultar el tipo de cambio");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ConsultarTipoCambio();
            WSMeteorologicoClient wS = new WSMeteorologicoClient("WSMeteorologico");
            Console.WriteLine(wS.efemerides(new efemerides()));
            airportsServiceClient ws = new airportsServiceClient("airportsServicePort");
            //airport[] aeropuertos = ws.airports("ed9a785b", "d8870be5ac62b2d6ddcfb041ea4aaf3d",2018,8,3,"");
            //foreach (airport item in aeropuertos)
            //{
            //    Console.WriteLine(item.name);
            //}
            //Console.WriteLine("Listo");
        }

        private void txtUSD_TextChanged(object sender, EventArgs e)
        {
            if (txtUSD.Focused)
            {
                if (txtUSD.Text.Length > 0)
                {
                    ConvertirMoneda(txtUSD.Text, txtCRC, true);
                }
                else
                {
                    txtCRC.Clear();
                }
            }
        }
        private void ConvertirMoneda(string text, TextBox txtResultado, bool sonUSD)
        {
            double valor = Double.Parse(text);
            double resultado = 0;
            resultado = sonUSD ? valor * tc : valor / tc;
            txtResultado.Text = Math.Round(resultado, 2).ToString();
        }

        private void txtCRC_TextChanged(object sender, EventArgs e)
        {
            if (txtCRC.Focused)
            {
                if (txtCRC.Text.Length > 0)
                {
                    ConvertirMoneda(txtCRC.Text, txtUSD, false);
                }
                else
                {
                    txtUSD.Clear();
                }
            }
        }
    }
}
