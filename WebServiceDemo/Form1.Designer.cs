﻿namespace WebServiceDemo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtFecha = new System.Windows.Forms.DateTimePicker();
            this.txtUSD = new System.Windows.Forms.TextBox();
            this.txtCRC = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dtFecha
            // 
            this.dtFecha.CalendarFont = new System.Drawing.Font("Symbol", 8.25F);
            this.dtFecha.CustomFormat = "dd/MM/yyyy";
            this.dtFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.875F);
            this.dtFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFecha.Location = new System.Drawing.Point(642, 12);
            this.dtFecha.Name = "dtFecha";
            this.dtFecha.Size = new System.Drawing.Size(263, 46);
            this.dtFecha.TabIndex = 2;
            this.dtFecha.ValueChanged += new System.EventHandler(this.dtFecha_ValueChanged);
            // 
            // txtUSD
            // 
            this.txtUSD.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.875F);
            this.txtUSD.Location = new System.Drawing.Point(237, 198);
            this.txtUSD.Name = "txtUSD";
            this.txtUSD.Size = new System.Drawing.Size(373, 61);
            this.txtUSD.TabIndex = 3;
            this.txtUSD.TextChanged += new System.EventHandler(this.txtUSD_TextChanged);
            // 
            // txtCRC
            // 
            this.txtCRC.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.875F);
            this.txtCRC.Location = new System.Drawing.Point(237, 278);
            this.txtCRC.Name = "txtCRC";
            this.txtCRC.Size = new System.Drawing.Size(373, 61);
            this.txtCRC.TabIndex = 4;
            this.txtCRC.TextChanged += new System.EventHandler(this.txtCRC_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.875F);
            this.label1.Location = new System.Drawing.Point(238, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(363, 55);
            this.label1.TabIndex = 5;
            this.label1.Text = "Tipo de Cambio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.875F);
            this.label2.Location = new System.Drawing.Point(616, 201);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 55);
            this.label2.TabIndex = 6;
            this.label2.Text = "USD";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.875F);
            this.label3.Location = new System.Drawing.Point(616, 278);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 55);
            this.label3.TabIndex = 7;
            this.label3.Text = "CRC";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 540);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCRC);
            this.Controls.Add(this.txtUSD);
            this.Controls.Add(this.dtFecha);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DateTimePicker dtFecha;
        private System.Windows.Forms.TextBox txtUSD;
        private System.Windows.Forms.TextBox txtCRC;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

