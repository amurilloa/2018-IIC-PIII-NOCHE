﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Persona
    {
        public int Id { get; set; }
        public int Cedula { get; set; }
        public string Nombre { get; set; }
        public string ApellidoUno { get; set; }

        private string apellidoDos;
        public string ApellidoDos
        {
            get { return apellidoDos?.ToUpper(); }
            set { apellidoDos = String.IsNullOrEmpty(value) ? "Desconocido" : value; }
        }
    }
}
