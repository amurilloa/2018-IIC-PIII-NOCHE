﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Operaciones
    {
        private int[] arreglo;

        /// <summary>
        /// Suma dos números enteros
        /// </summary>
        /// <param name="num1">Primer número entero</param>
        /// <param name="num2">Segundo número entero</param>
        /// <returns>Suma de ambos números</returns>
        public int Sumar(int num1, int num2)
        {
            return num1 + num2;
        }

        /// <summary>
        /// Resta dos números enteros
        /// </summary>
        /// <param name="num1">Primer número entero</param>
        /// <param name="num2">Segundo número entero</param>
        /// <returns>Resta del numero 2 al numero 1</returns>
        public int Restar(int num1, int num2)
        {
            return num1 - num2;
        }

        /// <summary>
        /// Multiplica dos números enteros
        /// </summary>
        /// <param name="num1">Primer número entero</param>
        /// <param name="num2">Segundo número entero</param>
        /// <returns>Multiplicación de ambos números</returns>
        public int Multiplicar(int num1, int num2)
        {
            return num1 * num2;
        }

        /// <summary>
        /// Division entre dos números enteros
        /// </summary>
        /// <param name="num1">Dividendo</param>
        /// <param name="num2">Divisor</param>
        /// <returns>Resultado de la division</returns>
        public double Dividir(double num1, double num2)
        {
            if (num2 != 0)
            {
                return num1 / num2;
            }
            else
            {
                throw new ArithmeticException("Division por 0 ");
            }
        }

        public void CrearArreglo(int largo)
        {
            arreglo = new int[largo];
        }

        public void AgregarElemento(int index, int numero)
        {
            if (index < arreglo.Length)
            {
                arreglo[index] = numero;
            }
        }

        public string ImprimirDatos()
        {
            string datos = "";
            foreach (int item in arreglo)
            {
                datos += String.Format("{0} ", item);
            }
            return datos;
        }


    }
}
