﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Program
    {
        static void Main(string[] args)
        {
            Operaciones ope = new Operaciones();
            int[] arreglo = { };
            string menu = "Menu Principal\n" +
                "1. Sumar\n" +
                "2. Restar\n" +
                "3. Multiplicar\n" +
                "4. Dividir\n" +
                "5. Llenar arreglo\n" +
                "6. Imprimir arreglo\n" +
                "7. Salir\n" +
                "Seleccione una opción: ";
            int op = -1;
            do
            {
                Console.Clear();
                Console.Write(menu);
                op = Int32.Parse(Console.ReadLine());
                int n1 = 0;
                int n2 = 0;

                if (op >= 1 && op <= 4)
                {
                    Console.Write("Digite el número 1: ");
                    n1 = Int32.Parse(Console.ReadLine());
                    Console.Write("Digite el número 2: ");
                    n2 = Int32.Parse(Console.ReadLine());
                }

                switch (op)
                {
                    case 1:
                        Console.WriteLine("Sumar {0}+{1}={2}", n1, n2, (ope.Sumar(n1, n2)));
                        break;
                    case 2:
                        Console.WriteLine("Restar {0}-{1}={2}", n1, n2, (ope.Restar(n1, n2)));
                        break;
                    case 3:
                        Console.WriteLine("Multiplicar {0}x{1}={2}", n1, n2, (ope.Multiplicar(n1, n2)));
                        break;
                    case 4:
                        try
                        {
                            Console.WriteLine("Dividir {0}/{1}={2:F2}", n1, n2, (ope.Dividir(n1, n2)));
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;
                    case 5:
                        Console.Write("Digite largo del arreglo: ");
                        n1 = Int32.Parse(Console.ReadLine());
                        //arreglo = new int[n1];
                        //for (int i = 0; i < arreglo.Length; i++)
                        //{
                        //    Console.Write("Digite el número de la posición {0}:", (i + 1));
                        //    arreglo[i] = Int32.Parse(Console.ReadLine());
                        //}
                        ope.CrearArreglo(n1);
                        for (int i = 0; i < n1; i++)
                        {
                            Console.Write("Digite el número de la posición {0}:", (i + 1));
                            ope.AgregarElemento(i, Int32.Parse(Console.ReadLine()));
                        }

                        break;
                    case 6:
                        //foreach (int item in arreglo)
                        //{
                        //    Console.Write("{0} ", item);
                        //}
                        Console.WriteLine(ope.ImprimirDatos());
                        break;
                    case 7:
                        Console.WriteLine("Gracias por usar la aplicación");
                        break;
                    default:
                        Console.WriteLine("Opción Inválida");
                        break;
                }
                Console.WriteLine("Presione cualquier para " + (op == 7 ? "salir" : "continuar"));
                Console.ReadKey();
            } while (op != 7);


            //Console.WriteLine("Presione cualquier tecla para salir....");
            //Console.ReadKey();
        }
    }

}
