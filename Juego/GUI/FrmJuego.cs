﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Juego.GUI
{
    public partial class FrmJuego : Form
    {
        public FrmJuego()
        {
            InitializeComponent();
        }

        private void FrmJuego_Load(object sender, EventArgs e)
        {
            GenerarTeclado();
        }

        private void GenerarTeclado()
        {
            flowLayoutPanel1.Controls.Clear();
            for (int i = 65; i < 91; i++)
            {
                Button b1 = new Button()
                {
                    Text = ((char)i).ToString(),
                    Height = 35,
                    Width = 35
                };
                b1.Tag = i;
                b1.Click += B1_Click;
                b1.TabStop = false;
                flowLayoutPanel1.Controls.Add(b1);
            }
        }

        private void B1_Click(object sender, EventArgs e)
        {
            PresionarTecla((Button)sender);
        }

        private void PresionarTecla(Button b)
        {
            int id = (int)b.Tag;
            if (b.Enabled)
            {
                b.Enabled = false;
                if (b.Text.Equals("Z"))
                {
                    GenerarTeclado();
                }
            }
        }

        private void BuscarTecla(object sender, KeyEventArgs e)
        {
            foreach (var item in flowLayoutPanel1.Controls)
            {
                if (item.GetType().IsInstanceOfType(new Button()) &&
                    ((int)((Button)item).Tag==e.KeyValue))
                {
                    PresionarTecla((Button)item);
                }
            }
        }
    }
}
