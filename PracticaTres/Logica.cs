﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaTres
{
    class Logica
    {
        /// <summary>
        /// Obtiene el máximo comun divisor de dos números enteros
        /// </summary>
        /// <param name="n1">número 1</param>
        /// <param name="n2">número 2</param>
        /// <returns>mcd</returns>
        internal int MCD(int n1, int n2)
        {
            int mcd = 1;
            int div = 2;
            while (n1 >= div && n2 >= div)
            {
                if (n1 % div == 0 && n2 % div == 0)
                {
                    mcd *= div;
                    n1 /= div;
                    n2 /= div;
                    div = 2;
                }
                else
                {
                    div++;
                }
            }
            return mcd;
        }

        /// <summary>
        /// Calcula la cantidad de conejos producidos en tantos meses
        /// </summary>
        /// <param name="meses">Cantidad de Meses a calcular</param>
        /// <returns>Cantidad de conejos</returns>
        internal int ConejosCan(int meses)
        {
            int fertiles = 1;
            int crias = 0;
            for (int i = 0; i < meses; i++)
            {
                int temp = fertiles;
                fertiles += crias;
                crias = temp;
            }
            return (crias + fertiles) * 2;
        }

        internal int ConejosMes(int can)
        {
            int meses = 0;
            int canConejos = 0;
            int fertiles = 1;
            int crias = 0;
            while (can > canConejos)
            {
                int temp = fertiles;
                fertiles = fertiles + crias;
                crias = temp;
                meses++;
                canConejos = (crias + fertiles) * 2;
            }

            return meses;
        }

        internal bool EsPrimo(int numero)
        {
            for (int i = 2; i < numero; i++)
            {
                if (numero % i == 0)
                {
                    return false;
                }
            }
            return true;
        }

        internal double CalcularPrimos(int cantidad)
        {
            DateTime di = DateTime.Now;
            int con = 0;
            int num = 0;
            while (con < cantidad)
            {
                if (EsPrimo(num))
                {
                    con++;
                }
                num++;
            }
            DateTime df = DateTime.Now;
            return df.Subtract(di).TotalSeconds;
        }

        internal bool EsPerfecto(int num)
        {
            int sum = 0;
            for (int i = 1; i < num; i++)
            {
                if (num % i == 0)
                {
                    sum += i;
                }
            }
            return num == sum;
        }
    }

}
