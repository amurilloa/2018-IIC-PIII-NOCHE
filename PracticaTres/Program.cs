﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaTres
{
    class Program
    {
        static int LeerInt(string mensaje = "Digite un número ")
        {
            FALLO:
            Console.Write(mensaje);
            if (!Int32.TryParse(Console.ReadLine(), out int num))
            {
                Console.Write("Intente nuevamente, ");
                mensaje = mensaje.ToLower();
                goto FALLO;
            }
            return num;
        }

        static void Main(string[] args)
        {
            Logica log = new Logica();

            string menu = "Menu Principal\n" +
                "1. Máximo Común Divisor\n" +
                "2. Granja de Conejos (cantidad)\n" +
                "3. Granja de Conejos (meses)\n" +
                "4. Es Primo\n" +
                "5. Tiempo en obtener 10000 primos\n" +
                "6. Es Perfecto\n" +
                "7. Salir\n" +
                "Seleccione una opción: ";

            int op = 0;
            do
            {
                Console.Clear();
                op = LeerInt(menu);
                switch (op)
                {
                    case 1:
                        int n1 = LeerInt("Digite el primer número: ");
                        int n2 = LeerInt("Digite el segundo número: ");
                        int mcd = log.MCD(n1, n2);
                        Console.WriteLine("El MCD de {0} y {1} es {2}", n1, n2, mcd);
                        break;
                    case 2:
                        int can = Math.Abs(LeerInt("Digite la cantidad de conejos deseada: "));
                        int tiempo = log.ConejosMes(can);
                        Console.WriteLine("Para producir {0} conejos necesitamos {1} {2}",
                            can, tiempo, (tiempo == 1 ? "mes" : "meses"));
                        break;
                    case 3:
                        int meses = Math.Abs(LeerInt("Digite la cantidad de meses: "));
                        int conejos = log.ConejosCan(meses);
                        Console.WriteLine("En {0} {1} producimos {2} conejos",
                            meses, (meses == 1 ? "mes" : "meses"), conejos);
                        break;
                    case 4:
                        int numero = Math.Abs(LeerInt("Digite el número a evaluar: "));
                        bool pri = log.EsPrimo(numero);
                        Console.WriteLine("El {0} {1} primo",numero, (pri ? "es":"no es"));
                        break;
                    case 5:
                        int cantidad = Math.Abs(LeerInt("Digite cantidad de primos a calcular: "));
                        double seg = log.CalcularPrimos(cantidad);
                        Console.WriteLine("Se tardaron {0}s en calcular {1} números primos",seg, cantidad);
                        break;
                    case 6:
                        int num = LeerInt("Digite el número a evaluar: ");
                        bool per = log.EsPerfecto(num);
                        Console.WriteLine("El {0} {1} perfecto", num, (per ? "es" : "no es"));
                        break;
                    case 7:
                        Console.WriteLine("Gracias por usar la aplicación");
                        break;
                    default:
                        break;
                }
                Console.ReadKey();
            } while (op != 7);

        }
    }
}
